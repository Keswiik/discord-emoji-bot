Setup:
  1. Install node.js
  2. Create a new file called "config.json"
      a) add the following text to it
          {
            "prefix": "+",
            "memes": {},
            "commands": {}
          }
  3. Open CMD, navigate to wherever you saved the bot.
  4. Execute "node bot.js"

Still a WIP so the code is rather messy. I'll clean it up later.
