var Discord = require("discord.js")
var request = require("request")
var crypto = require("crypto")
var util = require("util")
var sprintf = require("sprintf-js").sprintf
var bot = new Discord.Client()
var fs = require("fs")
var json = JSON.parse(fs.readFileSync("config.json", "utf8"))
var prefix = json["prefix"]
var memes = json["memes"]
var commands = json["commands"]
var lastChannel = undefined

bot.on("ready", function() {
  console.log("The memester is ready.");
});

bot.on("message", function(message) {
  if (message.content.startsWith(prefix)) {
    lastChannel = message.channel
    var command = message.content.slice(1).split(" ");
    /**
            MEMES
    */
    if (command[0] in memes) {
      console.log("Meme found");
      var meme = memes[command[0]];
      if (meme["type"] === "image" || meme["type"] == "gif") {
        console.log("Meme is an image");
        var files = fs.readdirSync("./memes/" + command[0])
        var file = "./memes/" + command[0] + "/" + files[(Math.random() * files.length) | 0]
        console.log(file);
        var image = fs.readFileSync(file)
        message.channel.sendFile(image, file.slice(file.lastIndexOf("/") + 1));
      } else if (meme["type"] = "text") {
        var reply = undefined;
        if (meme["usesMethod"]) {
          reply = eval(meme["method"])
        } else {
          reply = meme["reply"]
        }
        message.channel.sendMessage(reply);
      }
    /**
            COMMANDS
    */
    } else if (command[0] in commands) {
      if (command[0] == "addmeme") {
        addMeme(message.content.slice(1), message)
      } else if (command[0] == "editmemedesc") {
        editMemeDescription(message.content.slice(message.content.indexOf(" ") + 1))
      } else if (command[0] == "memes") {
        message.channel.sendMessage(getFormattedMemeList())
      }
    } else {
      message.channel.sendMessage("Unknown command.");
    }
  }
});

function squareE(text) {
  var sideLength = 1
  var spaces = 1
  while (spaces < text.length) {
    spaces = (sideLength * 2) + ((sideLength - 2) * 2)
    if (spaces < text.length) {
      sideLength += 1
    }
  }

  var textArr = []
  for (i = 0; i < sideLength; i++) {
    textArr.push([]);
    for (j = 0; j < sideLength; j++) {
      textArr[i].push(" ")
    }
  }

  space = 0;
  for (i = 0; i < 4; i++) {
    for (j = 0; j < sideLength - 1; j++) {
      if (i == 0) {
        textArr[0][j] = text.charAt(space)
      } else if (i == 1) {
        textArr[j][sideLength - 1] = text.charAt(space)
      } else if (i == 2) {
        textArr[sideLength - 1][sideLength - 1 - j] = text.charAt(space)
      } else if (i == 3) {
        textArr[sideLength - 1 - j][0] = text.charAt(space)
      }

      space += 1
      if (space == text.length) {
        break;
      }
    }
  }

  newText = "```\n"
  for (i = 0; i < textArr.length; i++) {
    for (j = 0; j < textArr.length; j++) {
      newText += textArr[i][j] + " "
    }
    newText += "\n"
  }
  newText += "```"

  return newText
}

function squareM(text) {
  var length = text.length
  var textArr = []
  for (i = 0; i < length; i++) {
    textArr.push([])
    for (j = 0; j < length * 2 - 1; j++) {
      textArr[i].push(" ")
    }
  }

  for (i = 0; i < length; i++) {
    if (i == 0) {
      for (j = 0; j < length; j++) {
        textArr[i][j * 2] = text.charAt(j)
      }
    } else if (i == length - 1) {
      for (j = 0; j < length; j++) {
        textArr[i][j * 2] = text.charAt(length - j - 1)
      }
    } else {
      textArr[i][0] = text[i]
      textArr[i][length * 2 - 2] = text.charAt(length - i - 1)
    }
  }

  newText = "```\n"
  for (i = 0; i < textArr.length; i++) {
    for (j = 0; j < textArr[i].length; j++) {
      newText += textArr[i][j]
    }
    newText += "\n"
  }
  newText += "```"

  return newText
}

function addMeme(text, message) {
  var data = text.split(" ");
  if (data.length != 4) {
    message.channel.sendMessage("Check your formatting. It's incorrect.")
    return false
  }

  if (data[1] in memes && data[2] == "image") {
    downloadImage(data[3],
      "./memes/" + data[1] + "/" + data[3].slice(data[3].lastIndexOf("/")))
  } else if (!(data[1] in memes) && data[2] == "image") {
    json.memes[data[1]] = {"type" : data[2]}
    dir = "./memes/" + data[1]
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir)
    }
    downloadImage(data[3],
      "./memes/" + data[1] + "/" + data[3].slice((data[3].lastIndexOf("/"))))
  }
}

function editMemeDescription(text) {
  var data = text.split(" : ")
  if (data[0] in memes) {
    json["memes"][data[0]].desc = data[1]
    updateConfig()
  } else {
    lastChannel.sendMessage("Meme does not exist.")
  }
}

function getFormattedMemeList() {
  var types = ["image", "text"]
  var formattedMessage = "```\nMemes\n"
  for (type of types) {
    formattedMessage += "\t" + type + "\n"
    for (var key in memes) {
      if (memes.hasOwnProperty(key) && memes[key].type == type) {
        formattedMessage += "\t\t- " + key + " : "
        meme = memes[key]
        if ("desc" in meme) {
          formattedMessage += meme["desc"]
        }
        formattedMessage += "\n"
      }
    }
  }
  formattedMessage += "```"
  return formattedMessage
}

function downloadImage(uri, filename) {
  request.head(uri, function(err, res, body) {
    console.log("content-type", res.headers["content-type"])
    console.log("content-length", res.headers["content-length"])
    if (!res.headers["content-type"].includes("image")) {
      lastChannel.sendMessage("Link type must be an image.")
      return
    }
    request(uri).pipe(fs.createWriteStream(filename)).on("close", function() {
      updateConfig()
    })
  })
}

function updateConfig() {
  fs.writeFile("config.json", JSON.stringify(json, null, 4), "utf8", function() {
    console.log("Configuration updated")
    loadConfig()
  })
}

function gayMeter(message) {
  console.log(message.mentions.users.size)
  var user = (message.mentions.users.size > 0) ? message.mentions.users.first() : message.author
  var length = 40
  var gayness = (Math.random() * 40) | 0
  var padding = Array(((user.length / 2) | 0) + 1).join(" ")
  if (padding.length < 2) {
    padding = "  "
  }
  var meter = "Here is how gay " + user + " is\n```\n"
  meter += Array((gayness + 1)).join(" ") + user.username + "\n"
  meter += padding + Array((gayness + 1)).join(" ") + "|\n"
  meter += padding + Array(length + 1).join("_") + "\n"
  meter += padding + sprintf("%-10s%-10s%-10s%-9s|\n", "|", "|", "|", "|")
  meter += padding + Array(length + 1).join("-") + "\n"
  meter += padding.slice(2) + "Ungay     kinda    moderate    very   rainbow\n"
  meter += "```"
  return meter
}

function loadConfig() {
  json = JSON.parse(fs.readFileSync("config.json", "utf8"))
  console.log(json);
  prefix = json["prefix"]
  memes = json["memes"]
  commands = json["commands"]
  console.log("New configuration loaded.");
  lastChannel.sendMessage("Meme configuration updated.")
}

 var login = fs.readFileSync("token.txt", "utf8")
 eval(login)
